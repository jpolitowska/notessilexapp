<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class NoteType extends AbstractType {
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('content', 'textarea', array(
        	'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5))),
        	'label' => 'Treść:',
        	'attr' => array('rows' => '6', 'class' => 'form-control'),
        ))
        ->add('publication_date', 'datetime', array(
        	'constraints' => array(new Assert\NotBlank()),
        	'label' => 'Data publikacji:',
        ));
	}
	
	public function getName()
	{
		return 'note';
	}
	
	public function getExtendedType()
	{
		return 'form';
	}
	
}

?>