<?php

// src/routes.php

$app->mount('/', new Controller\IndexController());
$app->mount('/notes', new Controller\NotesController());

?>