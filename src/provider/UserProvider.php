<?php

namespace Provider;
 
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\DBAL\Connection;
use Silex\Application;

class UserProvider implements UserProviderInterface
{
    private $conn;
    private $app;

    public function __construct(Connection $conn, Application $app)
    {
        $this->conn = $conn;
        $this->app = $app;
    }
 
    public function loadUserByUsername($username)
    {
        $user = $this->app['orm.em']->getRepository('Entity\User')->findOneBy(array('username' => $username));
        if (!$user) {
        	throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
		
        return new User($user->getUsername(), $user->getPassword(), explode(',', $user->getRole()), true, true, true, true);
    }
 
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
 
        return $this->loadUserByUsername($user->getUsername());
    }
 
    public function supportsClass($class)
    {
        return $class === 'Entity\User';
    }
    
    public function getCurrentUser()
    {
    	if ($this->isLoggedIn()) {
    		return $this->app['security']->getToken()->getUser();
    	}
    
    	return null;
    }
    
    function isLoggedIn()
    {
    	$token = $this->app['security']->getToken();
    	if (null === $token) {
    		return false;
    	}
    
    	return $this->app['security']->isGranted('IS_AUTHENTICATED_FULLY');
    }
}