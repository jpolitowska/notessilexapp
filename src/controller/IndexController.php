<?php 
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class IndexController implements ControllerProviderInterface {
	
	public function connect(Application $app) {
		
		$controllers = $app['controllers_factory'];
		
		// Home
		$controllers->get('/', function (Application $app) {
			return $app['twig']->render('index/index.html.twig');
		})
		->bind('home');
        
		// Login
		$controllers->get('/login', function(Request $request) use ($app) {
			return $app['twig']->render('index/login.html.twig', array(
				'error' => $app['security.last_error']($request),
				'last_username' => $app['session']->get('_security.last_username'),
		));
		})
		->bind('login');
        	
		return $controllers;
	}
}

?>