<?php 
namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Entity\Note;

class NotesController implements ControllerProviderInterface {
	
	public function connect(Application $app) {
		
		$controllers = $app['controllers_factory'];
		
		// List
		$controllers->get('/', function() use($app) {
			$em = $app['orm.em'];
			$notes = $em->getRepository('Entity\Note')->findAll();
			if (!$notes) {
				$app->abort(404, 'Nie znaleziono notatek.');
			}
        	
			return $app['twig']->render('notes/list.html.twig', array(
				"notes" => $notes
			));
		})
		->bind('notes_list');
		
		// Create
		$controllers->match('/new', function(Request $request) use($app) {
			$note = new Note();
			//default publication date
			$note->setPublicationDate(new \DateTime('now'));
        	
			$form = $app['form.factory']->createBuilder(new \Form\NoteType(), $note)
			->add('Dodaj', 'submit', array(
				'attr' => array('class' => 'btn btn-default')
			))
			->getForm();
			$form->handleRequest($request);
        	
			if ($form->isValid() && $request->isMethod('POST')) {
				$em = $app['orm.em'];
				$username = $app['user']->getUserName();
        		
				$user = $em->getRepository('Entity\User')->findOneBy(array('username' => $username));
				$note->setUser($user);
	        	
				$em->persist($note);
				$em->flush();
	        	
				$message = 'Notatka została dodana.';
				$app['session']->getFlashBag()->add('success', $message);
        	
				// redirect to notes list
				return $app->redirect($app['url_generator']->generate('notes_list'));
			}
        	
			return $app['twig']->render('notes/new.html.twig', array(
				'form' => $form->createView(),
				'title' => 'Dodaj notatkę',
			));
		})
		->bind('note_new');
        
		// Update
		$controllers->match('/edit/{id}', function($id, Request $request) use($app) {
			$em = $app['orm.em'];
			$note = $em->getRepository('Entity\Note')->find($id);
			if (!$note) {
				$app->abort(404, "Nie znaleziono notatki.");
			}
        	
			$username = $app['user']->getUserName();
			$username_note = $note->getUser()->getUsername();
        	
			// if user is owner a note
			if($username === $username_note) {
				$editForm = $app['form.factory']->createBuilder(new \Form\NoteType(), $note)
				->add('Zapisz', 'submit', array(
					'attr' => array('class' => 'btn btn-default')
				))
				->getForm();
				$editForm->handleRequest($request);
        		
				if ($editForm->isValid() && $request->isMethod('POST')) {
					$em->merge($note);
					$em->flush();
        		
					$message = 'Notatka została edytowana.';
					$app['session']->getFlashBag()->add('success', $message);
        			
					// redirect to notes list
					return $app->redirect($app['url_generator']->generate('notes_list'));
				}
			}
			else {
				$message = 'Nie jesteś właścicielem notatki.';
				$app['session']->getFlashBag()->add('warning', $message);
        		
				// redirect to notes list
				return $app->redirect($app['url_generator']->generate('notes_list'));
			}
        	
			return $app['twig']->render('notes/new.html.twig', array(
				'form' => $editForm->createView(),
				'title' => 'Edytuj notatkę',
			));
        	
		})
		->assert('id', '\d+')
		->bind('note_edit');
        
		// Delete
		$controllers->match('/delete/{id}', function($id) use ($app) {
			$em = $app['orm.em'];
			$note = $em->getRepository('Entity\Note')->find($id);
			if (!$note) {
				$app->abort(404, 'Nie znaleziono notatki.');
			}
        	
			$username = $app['user']->getUserName();
			$username_note = $note->getUser()->getUsername();
        	
			// if user is owner a note
			if($username === $username_note) {
				$em->remove($note);
				$em->flush();
        	
				$message = 'Notatka została usunięta.';
				$app['session']->getFlashBag()->add('success', $message);
			}else {
				$message = 'Nie jesteś właścicielem notatki.';
				$app['session']->getFlashBag()->add('error', $message);
			}
        	
			return $app->redirect($app['url_generator']->generate('notes_list'));
        	
		})
		->assert('id', '\d+')
		->bind('note_delete');
        
		return $controllers;
	}
}

?>