<?php

// src/app.php

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Provider\UserProvider;

$app = new Application();

// Doctrine DBAL
$app->register(new DoctrineServiceProvider(), array(
	'db.options' => array(
		'driver'	=> 'pdo_mysql',
		'host'	=> 'localhost',
		'dbname'	=> 'notessilexapp',
		'user'	=> 'root',
		'password'	=> '',
	),
));

// Doctrine ORM
$app->register(new DoctrineOrmServiceProvider, array(
	"orm.em.options" => array(
		"mappings" => array(
			array(
				"type"      => "yml",
				"namespace" => "Entity",
				"path"      => __DIR__."/config/doctrine",
			),
		),
	),
));

// TWIG
$app->register(new TwigServiceProvider(), array(
	'twig.path' => __DIR__.'/views',
));

// Url Generator
$app->register(new UrlGeneratorServiceProvider());

// Session
$app->register(new SessionServiceProvider());

// Security
$app['user.provider'] = $app->share(function($app) {
	return new UserProvider($app['orm.em']->getConnection(), $app);
});
$app->register(new SecurityServiceProvider(), array(
	'security.firewalls' => array(
		'secured' => array(
			'pattern' => '^.*',
			'anonymous' => true,
			'form' => array('login_path' => '/login', 'check_path' => '/notes/login_check'),
			'logout' => array('logout_path' => '/notes/logout'), 
			'users' =>  $app['user.provider'],
		),
	),
	'security.access_rules' => array(
		array('^/notes/.+$', 'ROLE_USER')
	)
));

// Form
$app->register(new FormServiceProvider());

// Translation
$app->register(new TranslationServiceProvider(), array(
	'locale_fallbacks' => array('pl'),
	'translator.messages' => array(),
));

// Validator
$app->register(new ValidatorServiceProvider());

// User
$app['user'] = $app->share(function($app) {
	return ($app['user.provider']->getCurrentUser());
});

// Routes
require __DIR__ . '/routes.php';

return $app;
	
?>